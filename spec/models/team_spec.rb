require 'rails_helper'

RSpec.describe Team, type: :model do
  it { is_expected.to belong_to(:master).class_name("User")  }
  it { is_expected.to have_many(:teammates).class_name("User")  }
  it { is_expected.to have_many(:meetings)  }

  it { is_expected.to validate_presence_of :name  }
end
