require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to belong_to(:working_team).class_name("Team").with_foreign_key(:team_id)  }
  it { is_expected.to have_one(:managed_team).class_name("Team").with_foreign_key(:master_id)  }
  it { is_expected.to have_many(:statuses)  }

  describe '#team' do
    context 'when user is a master' do
      it 'returns managed_team' do
        user = create :user
        team = user.create_managed_team(attributes_for(:team))
        expect(user.team).to eq team
      end
    end
    context 'when user is not a master' do
      it 'returns working_team' do
        team = create :team
        user = create :user, :teammate, working_team: team
        expect(user.team).to eq team
      end
    end
  end

  describe '#manage?' do
    context 'when user is a master' do
      context 'and it manages the team' do
        it 'returns true' do
          user = create :user
          team = user.create_managed_team(attributes_for(:team))
          expect(user.manage?(team)).to be_truthy
        end
      end
      context 'and it doesn\'t manage the team' do
        it 'returns false' do
          user = create :user
          team = create :team
          expect(user.manage?(team)).to be_falsey
        end
      end
    end
    context 'when user is not a master' do
      it 'returns nil' do
        team = create :team
        user = create :user, working_team: team
        expect(user.manage?(team)).to be_falsey
      end
    end
  end
end
