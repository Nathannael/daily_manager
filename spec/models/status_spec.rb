require 'rails_helper'

RSpec.describe Status, type: :model do
  it { is_expected.to belong_to :meeting  }
  it { is_expected.to belong_to(:teammate).class_name('User').with_foreign_key('user_id')  }

  it { is_expected.to validate_presence_of :message  }
  it do
    subject.meeting = create :meeting
    is_expected.to validate_uniqueness_of(:meeting_id).scoped_to(:user_id)
  end
end
