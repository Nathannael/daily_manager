require "rails_helper"

RSpec.describe StatusMailer, type: :mailer do
  describe "request_status" do
    let(:meeting) { FactoryBot.create :meeting }
    let(:user) { FactoryBot.build_stubbed :user, working_team: meeting.team, email: "user@example.org" }
    let(:mail) { StatusMailer.request_status(user, meeting) }

    it "renders the headers" do
      expect(mail.subject).to eq("Request status")
      expect(mail.to).to eq(["user@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match(" It's time for you to send your morning status update at Pied Piper!")
      expect(mail.body.encoded).to match(new_meeting_status_url(meeting))
    end
  end

end
