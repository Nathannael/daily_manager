# Preview all emails at http://localhost:3000/rails/mailers/status_mailer
class StatusMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/status_mailer/request_status
  def request_status
    meeting = FactoryBot.create :meeting
    user = FactoryBot.build_stubbed :user, working_team: meeting.team, email: "user@example.org"
    StatusMailer.request_status(user, meeting)
  end

end
