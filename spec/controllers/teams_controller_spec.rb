require 'rails_helper'

RSpec.describe TeamsController, type: :controller do
  let(:valid_attributes) {
    attributes_for(:team)
  }

  let(:invalid_attributes) {
    { name: nil }
  }

  before :each, :master_logged do
    master = create(:user)
    sign_in master
  end

  before :each, :teammate_logged do
    sign_in create(:user, :teammate)
  end

  describe "GET #show" do
    context 'if user is a scrum master', :master_logged do
      it "assings managed team" do
        subject.current_user.create_managed_team(name: "Hooli")
        get :show
        expect(assigns(:team).name).to eq 'Hooli'
      end
    end
    context 'if user is a teammate', :teammate_logged do
      it "assings team" do
        get :show
        expect(assigns(:team).name).to eq 'Pied Piper'
      end
    end
  end

  describe "GET #new" do
    context 'if user is a scrum master', :master_logged do
      context 'and it doesn\'t have a team yet' do
        it "returns a success response" do
          get :new
          expect(response).to be_successful
        end
      end

      context 'and it has a team' do
        it 'redirects to that team with message' do
          subject.current_user.create_managed_team(name: "Hooli")
          get :new
          expect(response).to redirect_to(team_path)
          expect(controller).to set_flash[:alert].to(/You already have a team./)
        end
      end
    end

    context 'if user is a teammate', :teammate_logged do
      it 'redirects to his team with error' do
        get :new
        expect(response).to redirect_to(team_path)
        expect(controller).to set_flash[:alert].to(/You are not a Scrum Master./)
      end
    end
  end

  describe "GET #edit" do
    context 'if user is a scrum master', :master_logged do
      context 'with a managed team' do
        it "assings managed team" do
          subject.current_user.create_managed_team(name: "Hooli")
          get :edit
          expect(assigns(:team).name).to eq 'Hooli'
        end
      end
      context 'without a managed team' do
        it "redirects to new" do
          get :edit
          expect(response).to redirect_to(new_team_path)
          expect(controller).to set_flash[:alert].to(/Please, create a team first./)
        end
      end
    end
    context 'if user is a teammate', :teammate_logged do
      it 'redirects to his team with error' do
        get :edit
        expect(response).to redirect_to(team_path)
        expect(controller).to set_flash[:alert].to(/You are not a Scrum Master./)
      end
    end
  end

  describe "POST #create" do
    context 'if user is a scrum master', :master_logged do
      context "with valid params" do
        it "creates a new Team" do
          expect {
            post :create, params: {team: valid_attributes}
          }.to change(Team, :count).by(1)
        end

        it "redirects to the created team" do
          post :create, params: {team: valid_attributes}
          expect(response).to redirect_to(team_path)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {team: invalid_attributes}
          expect(response).to render_template(:new)
        end
      end
    end
    context 'if user is a teammate', :teammate_logged do
      it 'redirects to his team with error' do
        post :create, params: {team: valid_attributes}
        expect(response).to redirect_to(team_path)
        expect(controller).to set_flash[:alert].to(/You are not a Scrum Master./)
      end
    end
  end

  describe "PUT #update" do
    context 'if user is a scrum master', :master_logged do
      context "with valid params" do
        let(:new_attributes) {
          {name: "new name"}
        }

        it "updates the requested team" do
          subject.current_user.create_managed_team(name: "Hooli")
          put :update, params: {team: new_attributes}
          expect(subject.current_user.team.name).to eq "new name"
        end

        it "redirects to the team" do
          subject.current_user.create_managed_team(name: "Hooli")
          put :update, params: {team: new_attributes}
          expect(response).to redirect_to(team_path)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          subject.current_user.create_managed_team(name: "Hooli")
          put :update, params: {team: invalid_attributes}
          expect(response).to render_template(:edit)
        end
      end
    end
    context 'if user is a teammate', :teammate_logged do
      it 'redirects to his team with error' do
        post :create, params: {team: valid_attributes}
        expect(response).to redirect_to(team_path)
        expect(controller).to set_flash[:alert].to(/You are not a Scrum Master./)
      end
    end
  end

  describe "DELETE #destroy" do
    context 'if user is a scrum master', :master_logged do
      it "destroys the requested team" do
        subject.current_user.create_managed_team(name: "Hooli")
        expect {
          delete :destroy
        }.to change(Team, :count).by(-1)
      end

      it "redirects to the teams list" do
        team = Team.create! valid_attributes
        delete :destroy, params: {id: team.to_param}
        expect(response).to redirect_to(new_team_path)
      end
    end
    context 'if user is a teammate', :teammate_logged do
      it 'redirects to his team with error' do
        post :create, params: {team: valid_attributes}
        expect(response).to redirect_to(team_path)
        expect(controller).to set_flash[:alert].to(/You are not a Scrum Master./)
      end
    end
  end

end
