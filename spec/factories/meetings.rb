FactoryBot.define do
  factory :meeting do
    team { create :team }
    time { :morning }
  end
end
