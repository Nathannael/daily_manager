FactoryBot.define do
  factory :user do
    email { Faker::Internet.safe_email }
    password { '12345678' }
    password_confirmation { '12345678' }

    trait :teammate do
      master { false }
      working_team { create :team }
    end
  end
end
