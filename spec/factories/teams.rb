FactoryBot.define do
  factory :team do
    name { "Pied Piper" }
    master { create :user }
  end
end
