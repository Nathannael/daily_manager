FactoryBot.define do
  factory :status do
    meeting {create :meeting }
    user {create :user }
    message {create "message" }
  end
end
