require "rails_helper"

RSpec.describe TeamsController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/team/new").to route_to("teams#new")
    end

    it "routes to #show" do
      expect(:get => "/team").to route_to("teams#show")
    end

    it "routes to #edit" do
      expect(:get => "/team/edit").to route_to("teams#edit")
    end


    it "routes to #create" do
      expect(:post => "/team").to route_to("teams#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/team").to route_to("teams#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/team").to route_to("teams#update")
    end

    it "routes to #destroy" do
      expect(:delete => "/team").to route_to("teams#destroy")
    end
  end
end
