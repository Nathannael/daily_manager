Rails.application.routes.draw do
  resource :team
  resources :meetings, only: :show do
    resources :statuses, except: [:index, :show]
  end
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "teams#show"
end
