To test the automatic email:

1. Update crontab:
  `whenever --update-crontab --set environment='development'`
2. install mailcatcher so every email will be intercepted and shown on the web interface:
`gem install mailcatcher`
3. run mailcatcher on console: `mailcatcher`
4. visit mailcatcher on http://127.0.0.1:1080/
