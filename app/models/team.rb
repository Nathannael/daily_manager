class Team < ApplicationRecord
  belongs_to :master, class_name: 'User'
  has_many :teammates, class_name: 'User'
  has_many :meetings

  validates_presence_of :name
end
