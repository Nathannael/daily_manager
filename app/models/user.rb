class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :working_team, class_name: "Team", required: false, foreign_key: "team_id"
  has_one :managed_team, class_name: "Team", foreign_key: "master_id"
  has_many :statuses

  def team
    master ? managed_team : working_team
  end

  def manage? team
    managed_team == team
  end
end
