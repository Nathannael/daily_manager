class Meeting < ApplicationRecord
  belongs_to :team
  has_many :statuses

  delegate :teammates, to: :team
  delegate :name, to: :team, prefix: :team

  enum time: [:morning, :afternoon]

  after_create :send_status_requests

  private
  def send_status_requests
    teammates.each do |teammate|
      StatusMailer.request_status(teammate, self).deliver_now
    end
  end
end
