class Status < ApplicationRecord
  belongs_to :meeting
  belongs_to :teammate, class_name: 'User', foreign_key: 'user_id'

  validates_presence_of :message
  validates_uniqueness_of :meeting_id, scope: :user_id
end
