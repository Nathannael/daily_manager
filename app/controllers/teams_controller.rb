class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  before_action :check_if_master, only: [:new, :create, :edit, :update, :destroy]
  before_action :check_if_has_team, only: [:edit, :update, :destroy]

  # GET /team
  # GET /team.json
  def show
  end

  # GET /team/new
  def new
    if current_user.team
      redirect_to team_path, alert: "You already have a team."
    else
      @team = current_user.build_managed_team
    end
  end

  # GET /team/edit
  def edit
  end

  # POST /team
  # POST /team.json
  def create
    @team = current_user.build_managed_team(team_params)
    respond_to do |format|
      if @team.save
        format.html { redirect_to team_path, notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /team
  # PATCH/PUT /team.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to team_path, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /team
  # DELETE /team.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to new_team_path, notice: 'Team was successfully destroyed. You need to create another one to continue using DailyManager' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = current_user.team
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name)
    end

    def check_if_master
      redirect_to team_path, alert: "You are not a Scrum Master." unless current_user.master?
    end

    def check_if_has_team
      redirect_to new_team_path, alert: "Please, create a team first." unless current_user.team
    end
end
