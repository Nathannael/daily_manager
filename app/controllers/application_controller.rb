class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  protected
=begin
    If user was created with a team token, then redirects them there;
    If user has no affiliation to a team yet, assume it's a new Scrum Master,
    and redirect to new team path.
=end
  def after_sign_up_path_for(resource)
    resource.team ? team_path : new_team_path
  end

  def after_sign_in_path_for(resource)
    resource.team ? team_path : new_team_path
  end
end
