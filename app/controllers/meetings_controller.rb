class MeetingsController < ApplicationController
  def show
    @meeting = current_user.team.meetings.find(params[:id])
  end
end
