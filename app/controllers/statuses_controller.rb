class StatusesController < ApplicationController
  before_action :set_meeting
  before_action :set_status, only: [:edit, :update, :destroy]
  before_action :check_if_teammate, only: [:new, :create]
  before_action :check_ownership, only: [:edit, :update, :destroy]

  # GET /statuses/new
  def new
    @status = @meeting.statuses.new
  end

  # GET /statuses/1/edit
  def edit
  end

  # POST /statuses
  # POST /statuses.json
  def create
    @status = @meeting.statuses.new(status_params.merge(teammate: current_user))

    respond_to do |format|
      if @status.save
        format.html { redirect_to @meeting, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @status }
      else
        format.html { render :new }
        format.json { render json: @status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statuses/1
  # PATCH/PUT /statuses/1.json
  def update
    respond_to do |format|
      if @status.update(status_params)
        format.html { redirect_to @status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @status }
      else
        format.html { render :edit }
        format.json { render json: @status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statuses/1
  # DELETE /statuses/1.json
  def destroy
    @status.destroy
    respond_to do |format|
      format.html { redirect_to statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status
      @status = Status.find(params[:id])
    end

    def set_meeting
      @meeting = Meeting.find(params[:meeting_id])
    end

    def check_if_teammate
      redirect_to root_path unless current_user.in? @meeting.teammates
    end

    def check_ownership
      redirect_to root_path unless @status.teammate == current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def status_params
      params.require(:status).permit(:message)
    end
end
