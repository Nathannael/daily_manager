class StatusMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.status_mailer.request_status.subject
  #
  def request_status(user, meeting)
    @greeting = "Hi, #{user.email}"
    @meeting = meeting
    @url = new_meeting_status_url(meeting)
    mail to: user.email
  end
end
