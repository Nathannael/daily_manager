# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
scrum_master = User.create!(email: 'admin@example.org', password: '123123123')
scrum_master.create_managed_team(name: 'Pied Piper').tap do |team|
  5.times { team.meetings.create(time: Meeting.times.keys.sample) }
  5.times do
    team.teammates.create(email: Faker::Internet.safe_email, password: '123123123',master: false).tap do |user|
      team.meetings.each do |meeting|
        user.statuses.create(meeting: meeting, message: Faker::Lorem.sentence(14))
      end
    end
  end
end
