class AddFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :team, foreign_key: true
    add_column :users, :master, :boolean, default: true
  end
end
