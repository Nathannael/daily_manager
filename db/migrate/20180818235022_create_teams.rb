class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.references :master, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
