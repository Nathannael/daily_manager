class CreateMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :meetings do |t|
      t.references :team, foreign_key: true
      t.integer :time

      t.timestamps
    end
  end
end
